describe ("Tickets", ()=> {
    //Antes de todos os testes
    beforeEach(()=> cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html"));
    //Input fields
    it("fills all the text input fields", () =>{
        cy.get("#first-name").type("Danilo");
        cy.get("#last-name").type("Correa");
        cy.get("#email").type("danilobcorrea@gmail.com");
        cy.get("#requests").type("Teste");
        cy.get("#signature").type("Danilo Correa");
    });
    //Select box
    it("Select two tickets", ()=>{
        cy.get("#ticket-quantity").select("2");
    });

    //Radio Button
    it("select 'vip' ticket type", () =>{
        cy.get("#vip").check();
    });
    // Checkbox
    it("selects 'social media' checkbox", ()=>{
        cy.get("#social-media").check();
    });

    it("selects 'friend' and 'publication', then uncheck 'friend'", ()=>{
        cy.get("#friend").check();
        cy.get("#publication").check();
        cy.get("#friend").uncheck();
    });
    //Validação de H1
    it("has 'TicketBox' header's heading", () =>{
        cy.get("header h1").should("contain", "TICKETBOX");
    });
    //verificação de alertas
    it("alerts on invalid email", ()=>{
        cy.get("#email").type("danilo.correa");
        cy.get("#email.invalid").should("exist"); //not.exist para negar a existência
    })
    //criação de alias para utilizar ao longo do código
    it("alerts on invalid email", ()=>{
        cy.get("#email")
        .as("email")
        .type("danilo.correa");
        cy.get("@email")
        .clear()
        .type("danilo@gmail.com")
    })

    it("putting it all together and then reseting it all", ()=>{
        const firstName = "Danilo";
        const lastName = "Correa";
        const fullName = `${firstName} ${lastName}`

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("teste@teste.com.br");

        cy.get("#ticket-quantity").select("2");
        cy.get("#vip").check();
        cy.get("#friend").check();
        cy.get("#requests").type("IPA beer");

        cy.get(".agreement p").should(
            "contain",
            `I, ${fullName}, wish to buy 2 VIP tickets`
        );
        cy.get("#agree").click();
        cy.get("#signature").type(fullName);
        cy.get("button[type='submit']")
        .as("submitButton")
        .should("not.be.disabled");
        cy.get("button[type='reset']").click();
        cy.get("@submitButton").should("be.disabled");
    });

    it("fills mandatory fields using support command", () =>{
        const customer = {
            firstName: "João",
            lastName: "Silva",
            email: "joaosilva@exempla.com"
        };
        cy.fillMandatoryFields(customer);
        cy.get("button[type='submit']")
        .as("submitButton")
        .should("not.be.disabled");

        cy.get("#agree").uncheck();

        cy.get("@submitButton").should("be.disabled");
    });
});